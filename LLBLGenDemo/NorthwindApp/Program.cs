﻿using System.Linq;
using LLBLGen_Northwind.DatabaseSpecific;
using LLBLGen_Northwind.Linq;
using LLBLGen_Northwind.EntityClasses;

namespace NorthwindApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ShowAllCustomersSorted();

            System.Console.WriteLine("******************\n");
            System.Console.WriteLine("New Customer - Contact:"); var contact = System.Console.ReadLine();
            System.Console.WriteLine("New Customer - Company:"); var company = System.Console.ReadLine();
            
            AddNewCustomers(contact, company);
            ShowCustomersByCompany(company);
        }

        private static void ShowCustomer(CustomerEntity customer)
        {
            System.Console.WriteLine(
                "Compamny: {1}; Customer: {0}", customer.ContactName, customer.CompanyName);
        }

        private static void ShowAllCustomers()
        {
            using (var adapter = new DataAccessAdapter())
                foreach (var customer in new LinqMetaData(adapter).Customer)
                    ShowCustomer(customer);
        }

        private static void ShowAllCustomersSorted()
        {
            using (var adapter = new DataAccessAdapter())
                foreach (var customer in new LinqMetaData(adapter).Customer.OrderBy(i => i.CompanyName + i.ContactName))
                    ShowCustomer(customer);
        }

        private static void ShowCustomersByCompany(string company)
        {
            using (var adapter = new DataAccessAdapter())
                foreach (var customer in
                                         from item in new LinqMetaData(adapter).Customer 
                                         where item.CompanyName == company
                                         select item)
                    ShowCustomer(customer);
        }

        private static void AddNewCustomers(string contact, string company)
        {
            var newCustomer = new CustomerEntity();
            newCustomer.CustomerId = company.Replace(" ", "").Substring(0, 5).ToUpper();
            newCustomer.ContactName = contact;
            newCustomer.CompanyName = company;
            
            using (DataAccessAdapter adapter = new DataAccessAdapter())
                adapter.SaveEntity(newCustomer);
        }
    }
}
