﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLBLGen_Northwind_2.EntityClasses;

namespace NorthwindApp2
{
    public class CustomerGuiContanier
    {
        public CustomerEntity Customer { get; set; }

        public CustomerGuiContanier() : base() { }
        public CustomerGuiContanier(CustomerEntity customer) : this()
        {
            this.Customer = customer;
        } 

        public override string ToString()
        {
            if (Customer != null)
                return String.Format("{0} - {1}", Customer.ContactName, Customer.CompanyName);
            else
                return base.ToString();
        }
    }
}
