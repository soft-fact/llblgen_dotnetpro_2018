﻿using System.Linq;
using System.ComponentModel;
using LLBLGen_Northwind_2;
using LLBLGen_Northwind_2.EntityClasses;
using LLBLGen_Northwind_2.CollectionClasses;
using System.Windows.Forms;
using System.Collections.Generic;

namespace NorthwindApp2
{
    public static class LLBLGenExtensions
    {
        public static CustomerCollection ShowAllCustomersSorted(this ListBox listBox)
        {
            var customers = new CustomerCollection();
            customers.GetMulti(null);
            customers.Sort((int)CustomerFieldIndex.ContactName, ListSortDirection.Ascending);

            listBox.AddCustomers(customers);
            if (listBox.Items.Count > 0)
                listBox.SelectedIndex = 0;

            return customers;
        }

        public static CustomerEntity AddNewCustomer(this ListBox listBox, string contact, string company)
        {
            var newCustomer = new CustomerEntity();
            newCustomer.CustomerId = company.Replace(" ", "").Substring(0, 5).ToUpper();
            newCustomer.ContactName = contact;
            newCustomer.CompanyName = company;
            newCustomer.Save();

            listBox.ShowAllCustomersSorted();
            listBox.SelectCustomer(newCustomer);
            return newCustomer;
        }

        public static void UpdateCustomer(this ListBox listBox, string contact, string company)
        {
            var customer = listBox.CurrentCustomer();
            if (customer != null)
            {
                customer.ContactName = contact;
                customer.CompanyName = company;
                customer.Save();
            }

             listBox.ShowAllCustomersSorted();
             listBox.SelectCustomer(customer);
        }

        public static bool SelectCustomer(this ListBox listBox, CustomerEntity customer)
        {
            for (int i = 0; i < listBox.Items.Count; i++)
                if (((CustomerGuiContanier)listBox.Items[i])
                        .Customer.CustomerId == customer.CustomerId)
                {
                    listBox.SelectedIndex = i;
                    return true;
                }

            return false;
        }

        public static void AddCustomers(this ListBox listBox, IEnumerable<CustomerEntity> customers)
        {
            listBox.Items.Clear();
            foreach (var customer in customers)
                listBox.AddCustomer(customer);
        }

        public static void AddCustomer(this ListBox listBox, CustomerEntity customer)
        {
            listBox.Items.Add(new CustomerGuiContanier(customer));
        }

        public static CustomerEntity CurrentCustomer(this ListBox listBox)
        {
            if (listBox.SelectedItem != null)
                return ((CustomerGuiContanier)listBox.SelectedItem).Customer;
            else
                return null;
        }

        public static OrderEntity CurrentOrder(this BindingSource bs)
        {
            if (bs.Current != null) return (OrderEntity)bs.Current;
            else return null;
        }
    }
}
