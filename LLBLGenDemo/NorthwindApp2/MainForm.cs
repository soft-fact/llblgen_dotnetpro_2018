﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LLBLGen_Northwind_2.EntityClasses;

namespace NorthwindApp2
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            listBoxCustomers.ShowAllCustomersSorted();
        }

        private void buttonAddContact_Click(object sender, EventArgs e)
        {
            listBoxCustomers.AddNewCustomer(textBoxContact.Text, textBoxCompany.Text);
        }

        private void buttonUpdateContact_Click(object sender, EventArgs e)
        {
            listBoxCustomers.UpdateCustomer(textBoxContact.Text, textBoxCompany.Text);
        }

        private void listBoxCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshCustomer();
        }

        private void bindingSourceOrders_PositionChanged(object sender, EventArgs e)
        {
            refreshOrderDetails();
        }

        private void refreshCustomer()
        {
            var customer = listBoxCustomers.CurrentCustomer();

            if (customer != null)
            {
                textBoxContact.Text = customer.ContactName;
                textBoxCompany.Text = customer.CompanyName;
                textBoxCustomerId.Text = customer.CustomerId;

                refreshOrders(customer);
            }
            else
            {
                textBoxContact.Text = "";
                textBoxCompany.Text = "";
                textBoxCustomerId.Text = "";
            }
        }
        
        private void refreshOrders(CustomerEntity customer)
        {
            if (customer != null)
            {
                bindingSourceOrders.DataSource = customer.GetMultiOrders(true);                
                refreshOrderDetails();
            }
            else
                bindingSourceOrders.DataSource = null;

        }

        private void refreshOrderDetails()
        {
            var order = bindingSourceOrders.CurrentOrder();
            if (order != null)
                bindingSourceOrderDetails.DataSource = order.GetMultiOrderDetailsExtended(true);
            else
                bindingSourceOrderDetails.DataSource = null;
        }
    }
}
