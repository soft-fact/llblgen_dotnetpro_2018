﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.3.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using LLBLGen_Northwind_2.EntityClasses;
using LLBLGen_Northwind_2.FactoryClasses;
using LLBLGen_Northwind_2.CollectionClasses;
using LLBLGen_Northwind_2.HelperClasses;
using LLBLGen_Northwind_2;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace LLBLGen_Northwind_2.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the Region Entity. It will perform database oriented actions for a entity of type 'RegionEntity'.</summary>
	public partial class RegionDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public RegionDAO() : base(InheritanceHierarchyType.None, "RegionEntity", new RegionEntityFactory())
		{
		}








		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
