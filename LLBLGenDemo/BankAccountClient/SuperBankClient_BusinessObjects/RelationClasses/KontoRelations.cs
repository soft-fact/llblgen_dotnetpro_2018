﻿//////////////////////////////////////////////////////////////
// <auto-generated>This code was generated by LLBLGen Pro 5.3.</auto-generated>
//////////////////////////////////////////////////////////////
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using LLBLGen_BankClient;
using LLBLGen_BankClient.FactoryClasses;
using LLBLGen_BankClient.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace LLBLGen_BankClient.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Konto. </summary>
	public partial class KontoRelations
	{
		/// <summary>CTor</summary>
		public KontoRelations()
		{
		}

		/// <summary>Gets all relations of the KontoEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TransaktionEntityUsingKontoId);
			toReturn.Add(this.KundeEntityUsingKundeId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between KontoEntity and TransaktionEntity over the 1:n relation they have, using the relation between the fields:
		/// Konto.Id - Transaktion.KontoId
		/// </summary>
		public virtual IEntityRelation TransaktionEntityUsingKontoId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Transaktionen" , true);
				relation.AddEntityFieldPair(KontoFields.Id, TransaktionFields.KontoId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KontoEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransaktionEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between KontoEntity and KundeEntity over the m:1 relation they have, using the relation between the fields:
		/// Konto.KundeId - Kunde.Id
		/// </summary>
		public virtual IEntityRelation KundeEntityUsingKundeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Kunde", false);
				relation.AddEntityFieldPair(KundeFields.Id, KontoFields.KundeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KundeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KontoEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticKontoRelations
	{
		internal static readonly IEntityRelation TransaktionEntityUsingKontoIdStatic = new KontoRelations().TransaktionEntityUsingKontoId;
		internal static readonly IEntityRelation KundeEntityUsingKundeIdStatic = new KontoRelations().KundeEntityUsingKundeId;

		/// <summary>CTor</summary>
		static StaticKontoRelations()
		{
		}
	}
}
