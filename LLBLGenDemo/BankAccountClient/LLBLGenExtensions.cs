﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using LLBLGen_BankClient;
using LLBLGen_BankClient.EntityClasses;
using LLBLGen_BankClient.CollectionClasses;
using System.Windows.Forms;
using System.Collections.Generic;

namespace BankAccountClient
{
    public static class LLBLGenExtensions
    {
        public static KundeCollection ShowAllClientsSorted(this ListBox listBox)
        {
            var clients = new KundeCollection();
            clients.GetMulti(null);
            clients.Sort((int)KundeFieldIndex.Name, ListSortDirection.Ascending);

            listBox.AddClients(clients);
            if (listBox.Items.Count > 0)
                listBox.SelectedIndex = 0;

            return clients;
        }

        public static KundeEntity AddNewClient(this ListBox listBox, 
            string address, DateTime birthday, string name, string phone, bool blocked)
        {
            var client = new KundeEntity();

            client.Adresse = address;
            client.Geburtsdatum = birthday;
            client.IstGesperrt = blocked;
            client.Name = name;
            client.Telefon = phone;
            client.Save();

            listBox.ShowAllClientsSorted();
            listBox.SelectClient(client);
            return client;
        }

        public static void UpdateCustomer(this ListBox listBox, 
            string phone, string address, bool blocked)
        {
            var client = listBox.CurrentClient();
            if (client != null)
            {
                client.Telefon = phone;
                client.Adresse = address;
                client.IstGesperrt = blocked;
                client.Save();
            }

            listBox.ShowAllClientsSorted();
            listBox.SelectClient(client);
        }

        public static bool SelectClient(this ListBox listBox, KundeEntity client)
        {
            for (int i = 0; i < listBox.Items.Count; i++)
                if (((ClientGuiContanier)listBox.Items[i])
                        .Client.Id == client.Id)
                {
                    listBox.SelectedIndex = i;
                    return true;
                }

            return false;
        }

        public static void AddClients(this ListBox listBox, KundeCollection clients)
        {
            listBox.Items.Clear();
            foreach (var client in clients)
                listBox.AddClient(client);
        }

        public static void AddClient(this ListBox listBox, KundeEntity client)
        {
            listBox.Items.Add(new ClientGuiContanier(client));
        }

        public static KundeEntity CurrentClient(this ListBox listBox)
        {
            if (listBox.SelectedItem != null)
                return ((ClientGuiContanier)listBox.SelectedItem).Client;
            else
                return null;
        }

        public static KontoEntity CurrentAccount(this BindingSource bs)
        {
            if (bs.Current != null) return (KontoEntity)bs.Current;
            else return null;
        }
    }
}
