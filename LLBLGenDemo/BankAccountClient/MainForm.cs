﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LLBLGen_BankClient;
using LLBLGen_BankClient.EntityClasses;

namespace BankAccountClient
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            listBoxClients.ShowAllClientsSorted();
        }

        private void buttonAddClient_Click(object sender, EventArgs e)
        {
            listBoxClients.AddNewClient(
                textBoxAddress.Text, dateTimePickerBirthDay.Value, textBoxName.Text, textBoxPhone.Text, checkBoxBlocked.Checked);
        }

        private void buttonUpdateClient_Click(object sender, EventArgs e)
        {
            listBoxClients.UpdateCustomer(textBoxPhone.Text, textBoxAddress.Text, checkBoxBlocked.Checked);
        }

        private void listBoxClients_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshClient();
        }

        private void bindingSourceAccounts_PositionChanged(object sender, EventArgs e)
        {
            refreshTransactions();
        }

        private void refreshClient()
        {
            var client = listBoxClients.CurrentClient();

            syncClientBinding();

            if (client != null)
                refreshAccounts(client);
        }

        private void syncClientBinding()
        {
            var client = listBoxClients.CurrentClient();

            if (client != null)
            {
                textBoxClientId.Text = Convert.ToString(client.Id);
                textBoxAddress.Text = client.Adresse;
                textBoxName.Text = client.Name;
                textBoxPhone.Text = client.Telefon;
                dateTimePickerBirthDay.Value = client.Geburtsdatum;
                checkBoxBlocked.Checked = client.IstGesperrt;
            }
            else
            {
                textBoxClientId.Text = default(string);
                textBoxAddress.Text = default(string);
                textBoxName.Text = default(string);
                textBoxPhone.Text = default(string);
                dateTimePickerBirthDay.Value = default(DateTime);
                checkBoxBlocked.Checked = default(bool);
            }
        }

        private void refreshAccounts(KundeEntity client)
        {
            if (client != null)
            {
                bindingSourceAccounts.DataSource = client.Konten;
                refreshTransactions();
            }
            else
                bindingSourceAccounts.DataSource = null;
        }

        private void refreshTransactions()
        {
            var account = bindingSourceAccounts.CurrentAccount();
            if (account != null)
                bindingSourceTransactions.DataSource = account.Transaktionen;
            else
                bindingSourceTransactions.DataSource = null;
        }

        private void dataGridViewAccounts_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            var account = (KontoEntity)bindingSourceAccounts.Current;
            if (account != null)
                account.Save();
        }

        private void dataGridViewTransactions_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            var tx = (TransaktionEntity)bindingSourceTransactions.Current;
            if (tx != null)
                tx.Save();
        }

    }
}
