﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLBLGen_BankClient.EntityClasses;

namespace BankAccountClient
{
    public class ClientGuiContanier
    {
        public KundeEntity Client { get; set; }

        public ClientGuiContanier() : base() { }
        public ClientGuiContanier(KundeEntity client)
            : this()
        {
            this.Client = client;
        } 

        public override string ToString()
        {
            if (Client != null)
                return String.Format("{0} - {1}", Client.Name, Client.Adresse);
            else
                return base.ToString();
        }
    }
}
