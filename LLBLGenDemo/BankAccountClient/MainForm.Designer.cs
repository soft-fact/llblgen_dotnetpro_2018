﻿namespace BankAccountClient
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTimePickerBirthDay = new System.Windows.Forms.DateTimePicker();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxBlocked = new System.Windows.Forms.CheckBox();
            this.textBoxClientId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonUpdateClient = new System.Windows.Forms.Button();
            this.buttonAddClient = new System.Windows.Forms.Button();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listBoxClients = new System.Windows.Forms.ListBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridViewAccounts = new System.Windows.Forms.DataGridView();
            this.ibanDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.istGesperrtDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.kundeIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saldoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valutaIsoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceAccounts = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTransactions = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kontoIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zeitstempelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IbanVon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IbanZu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txBetragDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txValutaIsoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.istBarDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.istOkDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.istStorniertDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fehlermeldungDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceTransactions = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTransactions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTransactions)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dateTimePickerBirthDay);
            this.panel1.Controls.Add(this.textBoxPhone);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.checkBoxBlocked);
            this.panel1.Controls.Add(this.textBoxClientId);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.buttonUpdateClient);
            this.panel1.Controls.Add(this.buttonAddClient);
            this.panel1.Controls.Add(this.textBoxAddress);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1659, 111);
            this.panel1.TabIndex = 3;
            // 
            // dateTimePickerBirthDay
            // 
            this.dateTimePickerBirthDay.Location = new System.Drawing.Point(806, 40);
            this.dateTimePickerBirthDay.Name = "dateTimePickerBirthDay";
            this.dateTimePickerBirthDay.Size = new System.Drawing.Size(305, 24);
            this.dateTimePickerBirthDay.TabIndex = 13;
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Location = new System.Drawing.Point(806, 72);
            this.textBoxPhone.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(305, 24);
            this.textBoxPhone.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(659, 76);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Phone";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(659, 44);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Birthday";
            // 
            // checkBoxBlocked
            // 
            this.checkBoxBlocked.AutoSize = true;
            this.checkBoxBlocked.Location = new System.Drawing.Point(449, 10);
            this.checkBoxBlocked.Name = "checkBoxBlocked";
            this.checkBoxBlocked.Size = new System.Drawing.Size(81, 22);
            this.checkBoxBlocked.TabIndex = 8;
            this.checkBoxBlocked.Text = "Blocked";
            this.checkBoxBlocked.UseVisualStyleBackColor = true;
            // 
            // textBoxClientId
            // 
            this.textBoxClientId.Location = new System.Drawing.Point(168, 8);
            this.textBoxClientId.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientId.Name = "textBoxClientId";
            this.textBoxClientId.ReadOnly = true;
            this.textBoxClientId.Size = new System.Drawing.Size(226, 24);
            this.textBoxClientId.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "ID";
            // 
            // buttonUpdateClient
            // 
            this.buttonUpdateClient.Location = new System.Drawing.Point(1160, 69);
            this.buttonUpdateClient.Margin = new System.Windows.Forms.Padding(4);
            this.buttonUpdateClient.Name = "buttonUpdateClient";
            this.buttonUpdateClient.Size = new System.Drawing.Size(190, 30);
            this.buttonUpdateClient.TabIndex = 5;
            this.buttonUpdateClient.Text = "Update Client";
            this.buttonUpdateClient.UseVisualStyleBackColor = true;
            this.buttonUpdateClient.Click += new System.EventHandler(this.buttonUpdateClient_Click);
            // 
            // buttonAddClient
            // 
            this.buttonAddClient.Location = new System.Drawing.Point(1160, 35);
            this.buttonAddClient.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAddClient.Name = "buttonAddClient";
            this.buttonAddClient.Size = new System.Drawing.Size(190, 30);
            this.buttonAddClient.TabIndex = 4;
            this.buttonAddClient.Text = "Add Client";
            this.buttonAddClient.UseVisualStyleBackColor = true;
            this.buttonAddClient.Click += new System.EventHandler(this.buttonAddClient_Click);
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(168, 72);
            this.textBoxAddress.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(440, 24);
            this.textBoxAddress.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 76);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Address";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(168, 40);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(440, 24);
            this.textBoxName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 44);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 111);
            this.panel2.Margin = new System.Windows.Forms.Padding(6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1659, 789);
            this.panel2.TabIndex = 4;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(6);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listBoxClients);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1659, 789);
            this.splitContainer1.SplitterDistance = 550;
            this.splitContainer1.SplitterWidth = 9;
            this.splitContainer1.TabIndex = 1;
            // 
            // listBoxClients
            // 
            this.listBoxClients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxClients.FormattingEnabled = true;
            this.listBoxClients.ItemHeight = 18;
            this.listBoxClients.Location = new System.Drawing.Point(0, 0);
            this.listBoxClients.Margin = new System.Windows.Forms.Padding(6);
            this.listBoxClients.Name = "listBoxClients";
            this.listBoxClients.Size = new System.Drawing.Size(550, 789);
            this.listBoxClients.TabIndex = 0;
            this.listBoxClients.SelectedIndexChanged += new System.EventHandler(this.listBoxClients_SelectedIndexChanged);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(6);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridViewAccounts);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridViewTransactions);
            this.splitContainer2.Size = new System.Drawing.Size(1100, 789);
            this.splitContainer2.SplitterDistance = 317;
            this.splitContainer2.SplitterWidth = 8;
            this.splitContainer2.TabIndex = 0;
            // 
            // dataGridViewAccounts
            // 
            this.dataGridViewAccounts.AutoGenerateColumns = false;
            this.dataGridViewAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAccounts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ibanDataGridViewTextBoxColumn,
            this.idDataGridViewTextBoxColumn,
            this.istGesperrtDataGridViewCheckBoxColumn,
            this.kundeIdDataGridViewTextBoxColumn,
            this.saldoDataGridViewTextBoxColumn,
            this.valutaIsoDataGridViewTextBoxColumn});
            this.dataGridViewAccounts.DataSource = this.bindingSourceAccounts;
            this.dataGridViewAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewAccounts.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewAccounts.Margin = new System.Windows.Forms.Padding(6);
            this.dataGridViewAccounts.Name = "dataGridViewAccounts";
            this.dataGridViewAccounts.Size = new System.Drawing.Size(1100, 317);
            this.dataGridViewAccounts.TabIndex = 2;
            this.dataGridViewAccounts.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAccounts_RowLeave);
            // 
            // ibanDataGridViewTextBoxColumn
            // 
            this.ibanDataGridViewTextBoxColumn.DataPropertyName = "Iban";
            this.ibanDataGridViewTextBoxColumn.HeaderText = "Iban";
            this.ibanDataGridViewTextBoxColumn.Name = "ibanDataGridViewTextBoxColumn";
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // istGesperrtDataGridViewCheckBoxColumn
            // 
            this.istGesperrtDataGridViewCheckBoxColumn.DataPropertyName = "IstGesperrt";
            this.istGesperrtDataGridViewCheckBoxColumn.HeaderText = "IstGesperrt";
            this.istGesperrtDataGridViewCheckBoxColumn.Name = "istGesperrtDataGridViewCheckBoxColumn";
            // 
            // kundeIdDataGridViewTextBoxColumn
            // 
            this.kundeIdDataGridViewTextBoxColumn.DataPropertyName = "KundeId";
            this.kundeIdDataGridViewTextBoxColumn.HeaderText = "KundeId";
            this.kundeIdDataGridViewTextBoxColumn.Name = "kundeIdDataGridViewTextBoxColumn";
            // 
            // saldoDataGridViewTextBoxColumn
            // 
            this.saldoDataGridViewTextBoxColumn.DataPropertyName = "Saldo";
            this.saldoDataGridViewTextBoxColumn.HeaderText = "Saldo";
            this.saldoDataGridViewTextBoxColumn.Name = "saldoDataGridViewTextBoxColumn";
            // 
            // valutaIsoDataGridViewTextBoxColumn
            // 
            this.valutaIsoDataGridViewTextBoxColumn.DataPropertyName = "ValutaIso";
            this.valutaIsoDataGridViewTextBoxColumn.HeaderText = "ValutaIso";
            this.valutaIsoDataGridViewTextBoxColumn.Name = "valutaIsoDataGridViewTextBoxColumn";
            // 
            // bindingSourceAccounts
            // 
            this.bindingSourceAccounts.DataSource = typeof(LLBLGen_BankClient.EntityClasses.KontoEntity);
            this.bindingSourceAccounts.PositionChanged += new System.EventHandler(this.bindingSourceAccounts_PositionChanged);
            // 
            // dataGridViewTransactions
            // 
            this.dataGridViewTransactions.AutoGenerateColumns = false;
            this.dataGridViewTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTransactions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.kontoIdDataGridViewTextBoxColumn,
            this.zeitstempelDataGridViewTextBoxColumn,
            this.IbanVon,
            this.IbanZu,
            this.txBetragDataGridViewTextBoxColumn,
            this.txValutaIsoDataGridViewTextBoxColumn,
            this.istBarDataGridViewCheckBoxColumn,
            this.istOkDataGridViewCheckBoxColumn,
            this.istStorniertDataGridViewCheckBoxColumn,
            this.fehlermeldungDataGridViewTextBoxColumn});
            this.dataGridViewTransactions.DataSource = this.bindingSourceTransactions;
            this.dataGridViewTransactions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTransactions.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewTransactions.Margin = new System.Windows.Forms.Padding(6);
            this.dataGridViewTransactions.Name = "dataGridViewTransactions";
            this.dataGridViewTransactions.Size = new System.Drawing.Size(1100, 464);
            this.dataGridViewTransactions.TabIndex = 1;
            this.dataGridViewTransactions.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTransactions_RowLeave);
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "TxId";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            // 
            // kontoIdDataGridViewTextBoxColumn
            // 
            this.kontoIdDataGridViewTextBoxColumn.DataPropertyName = "KontoId";
            this.kontoIdDataGridViewTextBoxColumn.HeaderText = "KontoId";
            this.kontoIdDataGridViewTextBoxColumn.Name = "kontoIdDataGridViewTextBoxColumn";
            // 
            // zeitstempelDataGridViewTextBoxColumn
            // 
            this.zeitstempelDataGridViewTextBoxColumn.DataPropertyName = "Zeitstempel";
            this.zeitstempelDataGridViewTextBoxColumn.HeaderText = "Zeitstempel";
            this.zeitstempelDataGridViewTextBoxColumn.Name = "zeitstempelDataGridViewTextBoxColumn";
            // 
            // IbanVon
            // 
            this.IbanVon.DataPropertyName = "IbanVon";
            this.IbanVon.HeaderText = "IbanVon";
            this.IbanVon.Name = "IbanVon";
            // 
            // IbanZu
            // 
            this.IbanZu.DataPropertyName = "IbanZu";
            this.IbanZu.HeaderText = "IbanZu";
            this.IbanZu.Name = "IbanZu";
            // 
            // txBetragDataGridViewTextBoxColumn
            // 
            this.txBetragDataGridViewTextBoxColumn.DataPropertyName = "TxBetrag";
            this.txBetragDataGridViewTextBoxColumn.HeaderText = "TxBetrag";
            this.txBetragDataGridViewTextBoxColumn.Name = "txBetragDataGridViewTextBoxColumn";
            // 
            // txValutaIsoDataGridViewTextBoxColumn
            // 
            this.txValutaIsoDataGridViewTextBoxColumn.DataPropertyName = "TxValutaIso";
            this.txValutaIsoDataGridViewTextBoxColumn.HeaderText = "TxValutaIso";
            this.txValutaIsoDataGridViewTextBoxColumn.Name = "txValutaIsoDataGridViewTextBoxColumn";
            // 
            // istBarDataGridViewCheckBoxColumn
            // 
            this.istBarDataGridViewCheckBoxColumn.DataPropertyName = "IstBar";
            this.istBarDataGridViewCheckBoxColumn.HeaderText = "IstBar";
            this.istBarDataGridViewCheckBoxColumn.Name = "istBarDataGridViewCheckBoxColumn";
            // 
            // istOkDataGridViewCheckBoxColumn
            // 
            this.istOkDataGridViewCheckBoxColumn.DataPropertyName = "IstOk";
            this.istOkDataGridViewCheckBoxColumn.HeaderText = "IstOk";
            this.istOkDataGridViewCheckBoxColumn.Name = "istOkDataGridViewCheckBoxColumn";
            // 
            // istStorniertDataGridViewCheckBoxColumn
            // 
            this.istStorniertDataGridViewCheckBoxColumn.DataPropertyName = "IstStorniert";
            this.istStorniertDataGridViewCheckBoxColumn.HeaderText = "IstStorniert";
            this.istStorniertDataGridViewCheckBoxColumn.Name = "istStorniertDataGridViewCheckBoxColumn";
            // 
            // fehlermeldungDataGridViewTextBoxColumn
            // 
            this.fehlermeldungDataGridViewTextBoxColumn.DataPropertyName = "Fehlermeldung";
            this.fehlermeldungDataGridViewTextBoxColumn.HeaderText = "Fehlermeldung";
            this.fehlermeldungDataGridViewTextBoxColumn.Name = "fehlermeldungDataGridViewTextBoxColumn";
            // 
            // bindingSourceTransactions
            // 
            this.bindingSourceTransactions.DataSource = typeof(LLBLGen_BankClient.EntityClasses.TransaktionEntity);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1659, 900);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "SuperBankClient - Customers, Accounts and Transactions";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTransactions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTransactions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxClientId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonUpdateClient;
        private System.Windows.Forms.Button buttonAddClient;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox listBoxClients;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dataGridViewAccounts;
        private System.Windows.Forms.DataGridView dataGridViewTransactions;
        private System.Windows.Forms.BindingSource bindingSourceAccounts;
        private System.Windows.Forms.BindingSource bindingSourceTransactions;
        private System.Windows.Forms.CheckBox checkBoxBlocked;
        private System.Windows.Forms.DateTimePicker dateTimePickerBirthDay;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn ibanDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn istGesperrtDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kundeIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn saldoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valutaIsoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kontoIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zeitstempelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IbanVon;
        private System.Windows.Forms.DataGridViewTextBoxColumn IbanZu;
        private System.Windows.Forms.DataGridViewTextBoxColumn txBetragDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn txValutaIsoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn istBarDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn istOkDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn istStorniertDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fehlermeldungDataGridViewTextBoxColumn;
    }
}

