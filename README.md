# README #

This README documents main steps, that are necessary to get demo applications up and running.

### What is this repository for? ###

* Demo-Project for LLBLGen dotnetpro-article (https://www.dotnetpro.de)
* 1.0

### License ###

This project is licensed under the MIT License - see the [MIT_License.md] file at (https://bitbucket.org/soft-fact/llblgen_dotnetpro_2018/src/) for details

### How do I get set up? ###

- Clone Source Code
- Install LLBLGen Pro (i.E. https://www.llblgen.com/pages/try.aspx)
- For the Demo 1-2 install Northwind-Database on the SQL Server (https://www.microsoft.com/en-us/download/details.aspx?id=23654)
- Configure connection string in App.config files for Demo 1-3 projects
- For Demo-3 generate DDL-Create-Scripts:
	
		1) Open file "LLBLGen_BankClient.llblgenproj" (Double-Click)
		2) Select VS-Menu "LLBLGen Pro -> Project -> Generate Database Schema Create Script (DDL SQL)"
		3) Click the Button "Edit Selected Task Specifics"
		4) In the next Popup-Dialog input ".\" in the "Destination root folder"-Textbox and click "OK"-Button
		5) Run generated Script in the destination SQL-Server
		

